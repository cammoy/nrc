import React from "react";
import { Route, Switch } from "react-router-dom";
import {
  Home,
  NotFound
} from "../views/";

// Build Routes
//-----------------------------------------------------------------------

const renderRoutes = (routes, props) => {
  if (routes.length) {
    return routes.map((item, key) => (
      <Route
        exact
        key={key}
        path={item.path}
        render={() => <item.component {...props} />}
      />
    ));
  }
  return [];
};

// Route Config
//-----------------------------------------------------------------------

const Routes = props => {
  const routeConfig = [
    {
      path: "/",
      component: Home
    },
    {
      path: "*",
      component: NotFound
    }
  ];

  return <Switch>{renderRoutes(routeConfig, props)}</Switch>;
};

export default Routes;
