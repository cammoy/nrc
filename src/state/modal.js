export default {
  state: { status: false, template: '' },
  reducers: {
    get(state, { status, template }) {
      return {
        status,
        template
      };
    }
  },
  effects: dispatch => ({
      toggle (payload) {
        dispatch.modal.get(payload);
      }
  })
}
