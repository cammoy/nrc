import {
  init
} from '@rematch/core';
import createLoadingPlugin from '@rematch/loading';
import updatedPlugin from '@rematch/updated';
import * as models from './models';
const loading = createLoadingPlugin({});
const updated = updatedPlugin();

const store = init({
  models,
  plugins: [loading, updated]
});

export default store;