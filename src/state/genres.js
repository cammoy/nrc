// Get a list of now playing movies

import CONFIG from '../config';
import axios from "axios";

export default {
  state: [],
  reducers: {
    get(state, payload) {
      return payload;
    }
  },
  effects: dispatch => ({
    async fetch(payload) {

        const getGenres = await axios.get(CONFIG.API.genres)
          .then(({ data: { genres }}) => genres )
          .catch(error => {
            console.log(error.response)
          });

      // Push results to state
      dispatch.genres.get(getGenres);
    }
  })
};
