// Get a list of now playing movies

import CONFIG from '../config';
import axios from "axios";

export default {
  state: [],
  reducers: {
    get(state, payload) {
      return payload;
    }
  },
  effects: dispatch => ({
    async fetch(payload) {

      // Holds all now playing movies
      let nowPlaying = [];

      // Get number of pages to loop over
      const totalPages = await axios.get(CONFIG.API.nowPlaying)
      .then(( { data: { total_pages } } ) => total_pages );

      // Loop over all pages
      for( var i = 1; i <= totalPages; i++) {
        const getMovies = await axios.get(`${CONFIG.API.nowPlaying}&page=${i}`)
          .then(({
            data: {
              results
            }
          }) => results)
          .catch(error => {
            console.log(error.response)
          });
        
        // Push movies to now playing array
        nowPlaying.push(getMovies);
      };

      // Push results to state
      dispatch.playing.get(nowPlaying.flat());
    }
  })
};

