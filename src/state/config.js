// Get a list of now playing movies

import CONFIG from '../config';
import axios from "axios";

export default {
  state: {},
  reducers: {
    get(state, payload) {
      return payload;
    }
  },
  effects: dispatch => ({
    async fetch(payload) {

      const getConfig = await axios.get(CONFIG.API.config)
        .then(({
          data: { images }
        }) => images )
        .catch(error => {
          console.log(error.response)
        });

      // Push results to state
      dispatch.config.get(getConfig);
    }
  })
};
