// Get movie trailer

import CONFIG from '../config';
import axios from "axios";

export default {
  state: {},
  reducers: {
    get(state, payload) {
      return payload;
    }
  },
  effects: dispatch => ({
    clear() {
      dispatch.trailer.get({});
    },
    async fetch(payload) {

      const getTrailer = await axios.get(CONFIG.API.trailer(payload))
        .then(({
          data: {
            results
          }
        }) => results)
        .catch(error => {
          console.log(error.response)
        });

      // Push results to state
      dispatch.trailer.get(getTrailer);
    }
  })
};
