export default {
    state: {
        rating: 3,
        genres: [],
    },
    reducers: {
        get(state, payload) {
            return { ...state, ...payload };
        }
    },
    effects: dispatch => ({
        clear() {
            dispatch.filters.get({
                rating: 3,
                genres: [],
            })
        },
        toggle(payload, rootState) {

            const { filters: { genres } } = rootState;
            const newFilters = genres;

            // Filter out genre if exists.

            if (newFilters.includes(payload)) {
                const removedExisting = newFilters.filter(e => e !== payload);

                dispatch.filters.get({
                    genres: removedExisting
                });
            
            // Add Genre to filter if exists

            } else {
                newFilters.push(payload);
                dispatch.filters.get({
                    genres: newFilters
                });
            }
        }
    })
}
