import React from 'react';
import { connect } from 'react-redux';
import Wrapper from './components/wrapper/';
import Overlay from './components/overlay/';
import Trailer from './components/trailer/';

const App = props => {

  const {
    modal,
    children,
  } = props;

  const setModalVisibility = template => {
    let tpl = template;
    tpl = template.length ? tpl : '';
    const visible = !modal.status;
    props.setModalVisibility({
      status: visible,
      template: tpl
    });
  };

  const modalContent = () => {
    const {
      template
    } = modal;
    if (template === 'trailer') 
      return <Trailer /> ;
    return false;
  }

  return (
    <Wrapper>
      {children}
      <Overlay
        key={1}
        visible={modal.status}
        onClose={() => setModalVisibility(false)}
        className={modal.template}>
        {modalContent()}
      </Overlay>
    </Wrapper>
  );
}

const mapState = state => ({
  modal: state.modal
});

const mapDispatch = dispatch => ({
  setModalVisibility: payload => dispatch.modal.toggle(payload),
});

export default connect(mapState, mapDispatch)(App);
