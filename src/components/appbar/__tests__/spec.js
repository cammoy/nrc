import { shallow, mount, render } from 'enzyme';
import React from 'react';
import Comp from '../';
import Menu from '../Menu';

const props = {
    items: [{
            title: 'Movies',
            href: '/'
        },
        {
            title: 'LinkedIn',
            href: 'https://www.linkedin.com/in/kwasib/',
            target: '_blank'
        },
        {
            title: 'Github',
            href: 'https://github.com/cammoy',
            target: '_blank'
        }
    ]
};

const wrapper = shallow(<Comp />);
const menuWrapper = shallow(<Menu {...props} />);

describe('<Header />', () => {

    it('should render the <AppBar /> component', () => {
        expect(wrapper.exists('.app-header')).to.equal(true);
    });

    it('should reder <Menu /> component once', () => {
        expect(wrapper.find(Menu)).to.have.length(1);
    });
});


describe('<Menu />', () => {
    it('should render 3 List Items', () => {
        expect(menuWrapper.find('li')).to.have.length(3);
    });

    it(`should have list items with class of 'navbar__menu__item'`, () => {
        expect(menuWrapper.find('.navbar__menu__item')).to.have.length(3);
    });
})