import React from 'react';
import Menu  from './Menu';
import './styles.scss';

const AppBar = ({ links }) => (
  <header className="col app-header">
    <div className="row">
      <h1 className="col-md-6 title">NRC - <span>New Release Club</span></h1>
      <nav className="col-md-6 navbar">
        <Menu items={links} />
      </nav>
    </div>
  </header>
);

export default AppBar;