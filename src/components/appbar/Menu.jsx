import React from 'react';
import PropTypes from 'prop-types';

const Item = ({ items }) => {
  const renderMenu = () => {
    let menuItems = [];

    if(items.length) {
      menuItems = items.map( ({ href, title, target}, key) => (
        <li className="navbar__menu__item" key={key}><a href={href} target={target ? target : null}>{title}</a></li>
      ));
    };
    return (
      <ul className="navbar__menu">
        {menuItems}
      </ul>
    );
  }
  return renderMenu();
}

Item.defaultProps = {
  items: []
};

Item.propTypes = {
  items:  PropTypes.arrayOf( PropTypes.object)
};

export default Item;