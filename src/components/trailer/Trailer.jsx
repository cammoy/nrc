import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

const Trailer = props => {

  console.log('props', props.trailers);

  const renderVideo = () => {
    if (props.trailers[0]) {
      const { key, name, type } = props.trailers[0];
      return (
        <Fragment>
          <div className="container-fluid">
            <div className="row">
              <div className="col-sm-7">
                <h3>{name}</h3>
                <h5>{type}</h5>
                <iframe width="100%" height="450" title="X Movie Trailer"
                src={`https://www.youtube.com/embed/${key}`} frameBorder="0" 
                allowFullScreen></iframe>
              </div>
              <div className="col-sm-5">
                <h3>You might also like</h3>
              </div>
            </div>
          </div>
        </Fragment>
      );
    }
  }

  return (
    <Fragment>
      {renderVideo()}
    </Fragment>
  )
};

Trailer.defaultProps = {
  site: 'YouTube'
};

Trailer.propTyeps = {
  key: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  site: PropTypes.string.isRequired
};

const mapState = state => ({
  trailers: state.trailer
});

const mapDispatch = dispatch => ({

});

export default connect(mapState, mapDispatch)(Trailer);