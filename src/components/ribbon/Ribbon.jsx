import React from 'react';
import PropTypes from 'prop-types';

const Ribbon = ({ label, position }) => (
  <div className={`ribbon ribbon--${position}`} >
    {label}
  </div>
);

Ribbon.propTypes = {
  label: PropTypes.string,
  position: PropTypes.string
};

Ribbon.defualtProps = {
  position: 'left',
  label: 'coming soon'
};

export default Ribbon;
