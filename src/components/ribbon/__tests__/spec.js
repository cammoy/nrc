import React from 'react';
import Ribbon from '../Ribbon';
import { shallow } from 'enzyme';

const props = {
  label: 'ribbon',
  position: 'right'
};

const wrapper = shallow(<Ribbon {...props} />);
describe('<Ribbon />', () => {
  it('should have ribbon class', () => {
    expect(wrapper.find('.ribbon')).to.have.length(1);
  });
  it(`should have label rendered as child`, () => {
    expect(wrapper.props().children).to.equal('ribbon');
  });
  it(`should pass "position" prop to class`, () => {
    expect(wrapper.find('.ribbon--right')).to.have.length(1);
  });
});