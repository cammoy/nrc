import React from 'react';
import AppBar from '../appbar/';
import Footer from '../footer/';

const links = [
  {
    title: 'Movies',
    href: '/'
  },
  {
    title: 'LinkedIn',
    href: 'https://www.linkedin.com/in/kwasib/',
    target: '_blank'
  },
  {
    title: 'Github',
    href: 'https://github.com/cammoy',
    target: '_blank'
  }
];
const Wrapper = ({ children }) =>  (
  <div className="container-fluid">
    <div className="row">
    <AppBar links={links}/>
    <div className="content">
      {children}
    </div>
    <Footer />
    </div>
  </div>
);

export default Wrapper;