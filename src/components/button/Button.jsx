import React from 'react';
import PropTypes from 'prop-types';
import './styles.scss';

const Button = ({ id, type, onClick, className, children }) => (
  <button 
    id={id}
    className={`btn ${className}`}
    type={type}
    onClick={onClick}>
    {children}
  </button>
);

Button.defaultProps = {
  type: "button",
  label: "Button Text"
};

Button.propTypes = {
  id: PropTypes.string.isRequired,
  type: PropTypes.oneOf(["submit", "reset", "button"])
};

export default Button;