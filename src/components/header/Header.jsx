/**
 *  @desc HTML Header Tag
 */

import React from "react";
import PropTypes from "prop-types";

const Header = ({ children, size, align }) => {
  return React.createElement(`h${size}`, {class: align}, children);
};

Header.defaultProps = {
  size: 1
};

Header.propTypes = {
  children: PropTypes.node.isRequired,
  size: PropTypes.oneOf([1, 2, 3, 4, 5, 6])
};

export default Header;
