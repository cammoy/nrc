import React from 'react';
import PropTypes from 'prop-types';

const Tag = ({ tag, id, className, onClick }) => (
  <li className={`tag ${className}`} onClick={() => onClick(id)} id={id}>
    {tag}
  </li>
);

Tag.propTypes = {
  tag: PropTypes.string.isRequired
};

export default Tag;

