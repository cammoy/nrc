import React from 'react';
import Tag from './Tag';
import PropTypes from 'prop-types';
import './styles.scss';

const Tags = ({ tags, onClick, filters }) => {
  
  const renderTags = () => {
    if (tags.length) {
      return tags.map( (tag, key) =>  {
        // Apply active class if filters.genres is passed down i.e it used as filters
        const activeClass = filters && filters.includes(tag.id) ? 'tag--active' :'';
        return <Tag key={key} id={tag.id} tag={tag.name} onClick={onClick} className={activeClass} />
      })
    };
    return null;
  }
  return (
    <ul className="tags">
      {renderTags()}
    </ul>
  )
};

Tags.defaultProps = {
  onClick: () => null,
  tags: []
};

Tags.propTypes = {
  tags: PropTypes.arrayOf(
    PropTypes.object
  ).isRequired,
  filters: PropTypes.arrayOf(
    PropTypes.number
  ),
  onClick: PropTypes.func
};

export default Tags;