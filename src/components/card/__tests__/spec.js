import { shallow } from 'enzyme';
import React from 'react';
import Cards from '../Cards';
import Card from '../Card';
import { cardData, cardsProps } from '../stub';


const cardsComp = shallow(<Cards {...cardsProps} />);
const CardComp = shallow(<Card {...cardData[0]} />);

describe('<Cards />', () => {

    it('should render the <Cards /> component', () => {
        expect(cardsComp.exists('.cards')).to.equal(true);
    });

    it(`should render correct number of <Card /> components`, () => {
        expect(cardsComp.find(Card)).to.have.length(cardData.length);
    });
    it(`should render .card class on <Card /> component(s)`, () => {
        expect(cardsComp.find('.cards')).to.have.length(cardData.length);
    });
});