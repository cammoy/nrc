export const cardData = [{
  vote_count: 588,
  id: 424139,
  video: false,
  vote_average: 6.6,
  title: 'Halloween',
  popularity: 253.778,
  poster_path: '/bXs0zkv2iGVViZEy78teg2ycDBm.jpg',
  original_language: 'en',
  original_title: 'Halloween',
  genre_ids: [
    27,
    53
  ],
  backdrop_path: '/tZ358Wk4BnOc4FjdGsiexAUvCMH.jpg',
  adult: false,
  overview: `Laurie Strode comes to her final confrontation with Michael Myers,
   the masked figure who has haunted her since she narrowly escaped his killing 
   spree on Halloween night four decades ago.`,
  release_date: '2018-10-19',
  config: {
    base_url: 'http://image.tmdb.org/t/p/',
    poster_sizes: [
      'w92',
      'w154',
      'w185',
      'w342',
      'w500',
      'w780',
      'original'
    ]
  }
}];

export const cardsProps = {
  data: [{
    title: 'test'
  }],
  config: {},
  genres: [],
};
