import React from 'react';
import PropTypes from 'prop-types';
import Tags from '../tag/';
import distanceInWordsStrict from 'date-fns/distance_in_words_strict';
import addDays from 'date-fns/add_days';
import format from 'date-fns/format';
import LazyLoad from 'react-lazy-load';
import './styles.scss';

const Card  = (props) => {
  const {
    id,
    title,
    poster_path,
    vote_average,
    popularity,
    votes,
    genres,
    overview,
    release_date,
    config,
    handleTrailer,
  } = props;

  const getImagePath = () => {
    const { base_url, poster_sizes } = config;
    return poster_path ? `${base_url}${poster_sizes[3]}${poster_path}` : 'https://via.placeholder.com/342x513'
  };

  // Friendly Film Relase date

  const fromNow = (releaseDate) =>  {
    const days = distanceInWordsStrict( new Date(releaseDate), Date.now());
    const format_release = format( new Date(releaseDate), 'Do MMM YYYY');
    let released = addDays(new Date(2014, 8, 1), days);

    return released = released > Date.now() ? 
    `in ${days} on ${format_release}` : 
    `${days} ago on ${format_release}`;
  }

  return (
    <div className="card col-12 col-sm-6 col-lg-4 col-xl-3">
      <div className="card__wrap">
        <div className="card__image">
        <LazyLoad
          offsetVertical={200} >
          <img src={getImagePath()} alt={title} />
        </LazyLoad>
          <span className="card__title">{title}</span>
        </div>
        <button onClick={() => handleTrailer(id) }>get trailer</button>
        <div className="card__content">
          <p>{overview}</p>
        </div>
        <div className="card__meta">
          <div className="card__meta__genres">
            <Tags tags={genres} />
          </div>
          <ul className="card__meta__details">
            <li>Rating: {vote_average}</li>
            <li>Release date: {fromNow(release_date)}</li>
            <li>Popularity: {popularity}</li>
            <li>Votes: {votes}</li>
          </ul>
        </div>
      </div>
    </div>
  );
};

Card.propTypes = {
  id: PropTypes.number,
  title: PropTypes.string,
  votes: PropTypes.number,
  popularity: PropTypes.number
};

export default Card;