import React from 'react';
import Card from './Card';
import PropTypes from 'prop-types';

const Cards = ({ data, config, genres, handleTrailer }) => {

  const getGenres = (genreList, genre_ids) => {

    if( genre_ids && genreList ) {
      return genreList.filter(item => {
        if( genre_ids.includes(item.id) ) return item;
        return false;
      })
    };
  };

  if( data.length ) {
    return (
      <div className="cards">
        {data.map( (item, key) => (
          <Card 
            key={key} {...item} 
            config={config}
            genres={getGenres(genres, item.genre_ids)}
            handleTrailer={handleTrailer}
          />)
        )}
      </div>
    )
  }
  return false;
};

Cards.propTypes = {
  data: PropTypes.arrayOf(PropTypes.object),
  config: PropTypes.object,
  genres: PropTypes.arrayOf(PropTypes.object)
};

export default Cards;