import React from 'react';
import PropTypes from 'prop-types'

const Input = ({ type, name, onClick, onChange }) => (
  <input type={type} name={name} onClick={onClick} onChange={onChange} />
);

Search.defaultProps  = {
  onClick: () => false,
  type: 'text'
};

Search.propTypes = {
  onChange: PropTypes.func.isRequired,
  onClick: PropTypes.func,
  name: PropTypes.string.isRequired,
  type: PropTypes.oneOf([ 'text', 'email', 'search', 'button' ])
}

export default Input;