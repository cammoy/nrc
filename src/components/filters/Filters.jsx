import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Tags from '../tag/';
import Slider from 'react-rangeslider';
import Button from '../button/';
// To include the default styles
import 'react-rangeslider/lib/index.css'
import './styles.scss';

const Filters = ({
    filters,
    genres,
    filterByGenre,
    filterByRating,
    clearFilters,
  }) => { 

  const formatRating = value => `${value}`;

  const sliderLabels = {
    0: 'Low',
    5: 'Medium',
    10: 'High'
  };

  return (
    <div className="filters">
      <h3 className="filters__heading"> Filters </h3>
      Rating:
      <Slider
        value={filters.rating}
        step={0.5}
        min={0}
        max={10}
        labels={sliderLabels}
        format={formatRating}
        handleLabel={filters.rating.toString()}
        onChange={ payload => filterByRating({ rating: payload })}
      />
      <Tags tags={genres} onClick={ payload => filterByGenre(payload)} filters={filters.genres} />
      <Button id="clear" className="btn--close" onClick={ () => clearFilters()}>
        clear filters
      </Button>
    </div>
  );
};

Filters.propTypes = {
  filters: PropTypes.object
};

const mapState = state => ({
  filters: state.filters,
  genres: state.genres
});

const mapDispatch = dispatch => ({
  filterByRating: payload => dispatch.filters.get(payload),
  filterByGenre: payload => dispatch.filters.toggle(payload),
  clearFilters: () => dispatch.filters.clear()
});

export default connect(mapState, mapDispatch)(Filters);