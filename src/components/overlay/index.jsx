import React from 'react';
import PropTypes from 'prop-types';
import Rodal from 'rodal';
import './rodal.scss';
import './style.scss';

const Modal = ({ visible, children, onClose, className }) => {
  const styles = {
    width: '100%',
    height: '100%'
  };

  if (visible) {
    return (
      <Rodal
        visible={visible}
        onClose={onClose}
        animation="door"
        customStyles={styles}
        className={`s_modal-${className}`}
      >
        {children}
      </Rodal>
    );
  }
  return null;
};

Modal.defaultProps = {
  className: ''
};

Modal.propTypes = {
  children: PropTypes.node.isRequired,
  visible: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
  className: PropTypes.string
};

export default Modal;
