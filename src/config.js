// API Configuration file keeps commonly used vars in one place

const key = '0d5a93b947675e2f6838afc477fba1b0';

export  default {
  API: {
    key: key,
    trailer: id => `https://api.themoviedb.org/3/movie/${id}/videos?api_key=${key}&language=en-US`,
    nowPlaying: `https://api.themoviedb.org/3/movie/now_playing?api_key=${key}&language=en-GB&region=GB`,
    genres: `https://api.themoviedb.org/3/genre/movie/list?api_key=${key}&language=en-GB`,
    config: `https://api.themoviedb.org/3/configuration?api_key=${key}`
  }
};

