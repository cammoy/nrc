// Setup Chia Assertions
import { expect } from '../src/utils/testUtils';
global.jestExpect = global.expect; 
global.expect = expect;


// Setup Enzyme
import { configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
configure({
  adapter: new Adapter()
});