import React, { Component } from 'react';
import { connect } from "react-redux";
import Filters from '../components/filters/';
import Cards from '../components/card/';
import LazyLoad from 'react-lazy-load';


class Home extends Component {

  componentDidMount() {
    this.props.getNowPlaying();
    this.props.getGenres();
    this.props.getConfig();
  }

  handleTrailer = (movieId) => {
    this.props.removeTrailer();
    this.props.getTrailer(movieId);
    this.props.modal({
      status: true,
      template: 'trailer'
    });
  }

  // Combined Card Filters
  //--------------------------------------------------------------------------

  filteredCards = (cards) => {
    const { genres } = this.props.filters;
    if( cards ) {

      // Order By 'popularity' key in descending order i.e most popular first

      const movies = cards.sort(function (a, b) {
        return a.popularity > b.popularity;
      });

      // Combile vote_average and genre filers
      
      return movies.filter(card => {
        if (
          card.vote_average >= this.props.filters.rating && 
          genres.every(e => card.genre_ids.includes(e))) {
            return card;
        }
        return false;
      })
    }
  };

  render() {   

    return (
      <div className="container-fluid">
        <div className="row">
          <div className="sidebar col-12 col-md-4 col-lg-3 col-xl-2">
           <Filters />
          </div>  
          <div className="col-12 col-md-8 col-lg-9 col-lg-3 col-xl-10">
            <Cards 
              data = {
                this.filteredCards(this.props.playing)
              }
              config={this.props.config} 
              genres={this.props.genres}
              handleTrailer={this.handleTrailer}
            />
          </div>
        </div>
      </div>
    )
  }
};

const mapState = state => ({
  genres: state.genres,
  playing: state.playing,
  filters: state.filters,
  config: state.config,
  trailer: state.trailer,
});

const mapDispatch = dispatch => ({
  getNowPlaying: () => dispatch.playing.fetch(),
  getGenres: () => dispatch.genres.fetch(),
  getConfig: () => dispatch.config.fetch(),
  getTrailer: payload => dispatch.trailer.fetch(payload),
  removeTrailer: () => dispatch.trailer.clear(),
  modal: payload => dispatch.modal.toggle(payload)
});

export default connect(mapState, mapDispatch)(Home);